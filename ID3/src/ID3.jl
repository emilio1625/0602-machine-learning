module ID3

using Tree, DataFrames

export id3

function id3(set::DataFrame, attributes::Vector{Symbol}, objective::Symbol)
    ent, nS = entropy(set, objective)
    if ent == 0
        return Node(first(getproperty(set, objective)))
    elseif size(attributes,1) == 0
        return Node("Desconocido")
    else
        gains = map(attr -> gain(set, attr, objective), attributes)
        winner = attributes[argmax(gains)]
        tree = Node(string(winner))
        for val in unique(getproperty(set, winner))
            cond = x -> getproperty(x, winner) == val
            child = Node(val, cond, [id3(filter(cond, set),
                                         filter(x -> x ≠ winner, attributes),
                                             objective)])
            push!(tree.children, child)
        end
        return tree
    end
end

function gain(set::DataFrame, attr::Symbol, objective::Symbol)
    E, nS = entropy(set, objective)
    sum = 0
    for val in unique(getproperty(set,attr))
        ent, nA = entropy(filter(x -> getproperty(x, attr) == val, set),
                          objective)
        sum += (nA/nS)*ent
    end
    return E - sum
end

function entropy(set::DataFrame, attr::Symbol)::Tuple{Float64, Int64}
    nS = size(set, 1)
    sum = 0
    for class in unique(getproperty(set, attr))
        A = filter(x -> getproperty(x, attr) == class, set)
        nA = size(A, 1)
        if nA == 0 break end
        sum -= (nA/nS)log2(nA/nS)
    end
    return sum, nS
end

end

