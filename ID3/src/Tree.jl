module Tree

export Node, evaltree, showtree

struct Node
    label::AbstractString
    cond::Function
    children::Vector{Node}
end

Node(label::AbstractString) = Node(label, x -> true, [])

Node(label::AbstractString, cond::Function) =
        Node(label, cond, [])

function evaltree(node::Node, data)
    if length(node.children) == 0
        return node.label
    elseif node.cond(data) != false
        for child in node.children
            ans = evaltree(child, data)
            ans != false && return ans
        end
    else
        return false
    end
end

let idx = 1, flag = false
    x = ["A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M" "N" "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z"]
    global shownode, resetshow
    function shownode(io::IO, node::Node)
        if idx > length(x) - 1 idx = 1 end
        if isempty(node.children) println(io, "$(x[idx])[$(node.label)];") end
        z = idx
        for child in node.children
            if !flag
                print(io, "$(x[z])[$(node.label)] -- $(child.label)")
            else
                idx += 1
                println(io, " --> $(x[idx])[$(child.label)];")
            end
            flag = !flag
            shownode(io, child)
        end
    end
    function resetshow() idx=1 end
end

function showtree(io::IO, node::Node)
    println(io, "graph TD;")
    shownode(io, node)
    resetshow()
end

Base.show(io::IO, node::Node) = showtree(io, node)

end
